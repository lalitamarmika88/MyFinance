package android.lalita.com.myfi;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by lalita on 4/10/2018.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.holder> {
   private Context context;
   public List<AddTransc> list;

    public Adapter(Context context, List<AddTransc>list) {
        this.context=context;
        this.list=list;
    }

    @Override
    public Adapter.holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recview_transc, parent, false);
        holder hldr = new holder(view);
        return hldr;
    }

    @Override
    public void onBindViewHolder(Adapter.holder holder, int position) {
        AddTransc data = list.get(position);
        holder.type.setText(data.getType());
        if(data.getType().equals("Pengeluaran")){
            holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_down));
            holder.gambar.setColorFilter(ContextCompat.getColor(context, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        }else{
            holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_up));
            holder.gambar.setColorFilter(ContextCompat.getColor(context, R.color.baseyellow), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        holder.desc.setText(data.getDescription());
        holder.amount.setText("Rp. "+NumberFormat.getInstance(Locale.US).format(data.getAmount()));
        holder.date.setText(data.getDate());

    }

    public AddTransc getData(int position){
        return list.get(position);
    }

    public void deleteData(int i){
        list.remove(i);
        notifyItemRemoved(i);
        notifyItemRangeChanged(i, list.size());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class holder extends RecyclerView.ViewHolder{
        public TextView type, desc, amount, date;
        public CardView cardView; ImageView gambar;

        public holder(View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.tipegambar);
            type = itemView.findViewById(R.id.recordType);
            desc = itemView.findViewById(R.id.recordDesc);
            amount = itemView.findViewById(R.id.recordAmount);
            date = itemView.findViewById(R.id.recordDate);
        }
    }
}
