package android.lalita.com.myfi;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {
    private TextView tv;
    private ImageView iv;
    FirebaseAuth auth; FirebaseAuth.AuthStateListener listen;
    Intent i;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        tv = (TextView) findViewById(R.id.tv);
        Typeface custfont = Typeface.createFromAsset(getAssets(),"fonts/sweet.ttf");
        tv.setTypeface(custfont);
        iv = (ImageView) findViewById(R.id.iv);
        auth = FirebaseAuth.getInstance();
        listen = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user!=null){
                    i = new Intent(SplashScreen.this, Home.class);
                }else{
                    i = new Intent(SplashScreen.this, SignIn.class);
                }
            }
        };
        auth.addAuthStateListener(listen);

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.transition);
        tv.startAnimation(anim);
        iv.startAnimation(anim);
        Thread timer = new Thread(){
            public void run(){
                try {
                    sleep(2000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();
    }

}
