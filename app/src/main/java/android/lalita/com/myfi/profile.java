package android.lalita.com.myfi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.Locale;

public class profile extends AppCompatActivity {
    ImageView iv;
    TextView nama, saldo;
    DatabaseReference dataref;
    StorageReference storef;
    SharedPreferences shp;
    ProgressDialog pd;
    User cur;
    String currentiduser;
    Intent galleryintent, CropIntent;
    Uri uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        nama = findViewById(R.id.gantinamaprofile);
        saldo = findViewById(R.id.gantisaldoprofile);
        iv = findViewById(R.id.gantiprofile);
        pd = new ProgressDialog(this);

        dataref = FirebaseDatabase.getInstance().getReference().child("user");
        storef = FirebaseStorage.getInstance().getReference();
        shp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        currentiduser = shp.getString("key", null);

        nama.setText(shp.getString("id", null));
        saldo.setText("Rp. "+ NumberFormat.getInstance(Locale.US).format(shp.getInt("saldo", 0)));
        if (shp.getString("gambar", null) != null) {
            Glide.with(profile.this).load(shp.getString("gambar", null)).into(iv);
        }

        Query baru = dataref.orderByKey().equalTo(currentiduser).limitToFirst(1);
        baru.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    cur = data.getValue(User.class);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void gantigambarprofile(View view) {
        galleryintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleryintent, "Select photo from library"), 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==2){
            if (data!=null){
                uri = data.getData();
//                cropimage();
                Bitmap bit = null;
                try {
                    bit = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                pd.setTitle("Updating Profile");
                pd.setMessage("Please wait, the image is loading");
                pd.show();
                updateprofileimage(bit);
                iv.setImageBitmap(bit);
            }
        }
        else if (requestCode==1){
            Uri baa = data.getData();
            try{
                Bitmap bit = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                pd.setTitle("Updating Profile");
                pd.setMessage("Please wait, the image is loading");
                pd.show();
                updateprofileimage(bit);
                iv.setImageBitmap(bit);
            }catch (Exception e){
                Log.w("Error: ", e.getMessage());
            }
        }
        else{
            Toast.makeText(this, "Updating failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateprofileimage(Bitmap bit) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        final byte[] data = baos.toByteArray();
        UploadTask upload = storef.child("profileimage/"+cur.getId()).putBytes(data);
        upload.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                cur.setGambar(taskSnapshot.getDownloadUrl().toString());
                dataref.child(currentiduser).updateChildren(cur.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(profile.this, "Profile photo updated!", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor edit = shp.edit();
                        edit.putString("gambar", cur.getGambar());
                        edit.commit();
                        pd.dismiss();
                    }
                });
            }
        });

    }

    private void cropimage() {
        CropIntent = new Intent("com.android.camera.action.CROP");
        CropIntent.setDataAndType(uri,"image/*");
        CropIntent.putExtra("crop", "true");
        CropIntent.putExtra("outputX", 500);
        CropIntent.putExtra("outputY", 500);
        CropIntent.putExtra("aspectX", 3);
        CropIntent.putExtra("aspectY", 3);
        CropIntent.putExtra("scaleUpIfNeeded", true);
        CropIntent.putExtra("return-data", true);

        File a = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), String.valueOf(System.currentTimeMillis())+".jpg");
        Uri b = Uri.fromFile(a);

        CropIntent.putExtra(MediaStore.EXTRA_OUTPUT, b);
        startActivityForResult(CropIntent,1);
    }
}

