package android.lalita.com.myfi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class record extends AppCompatActivity {
    RecyclerView recV;
    Adapter adapt, currentadaapter;
    ArrayList<AddTransc> list;
    DatabaseReference dataref;
    CollapsingToolbarLayout ctl;
    SharedPreferences shp; SharedPreferences.Editor edit;
    Spinner filtertipe, filterbulan;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        recV = findViewById(R.id.iniRecViewnya);
        ctl = findViewById(R.id.inicollaptolbarrecord);
        filtertipe = findViewById(R.id.filterTipe);
        filterbulan = findViewById(R.id.filterBulan);
        list = new ArrayList<>();
        dataref = FirebaseDatabase.getInstance().getReference().child("transaksi");
        shp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        edit = shp.edit();

        ctl.setTitle("Rp. "+ NumberFormat.getInstance(Locale.US).format(shp.getInt("saldo", 0)));

        adapt = new Adapter(this, list);
        recV.setHasFixedSize(true); recV.setLayoutManager(new LinearLayoutManager(this));
        recV.setAdapter(adapt);
        dataref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    AddTransc current = data.getValue(AddTransc.class);
                    if(current.getUser().equals(shp.getString("id", null))){
                        list.add(current);
                        adapt.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        filtertipe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Adapter adabterbaru = adapt;
                switch (adapterView.getSelectedItem().toString()){
                    case "None":
                        recV.setAdapter(adapt);
                        break;
                    case "Pemasukan":
                        List<AddTransc> listmasuk = new ArrayList<>();
                        for(AddTransc current : adabterbaru.list){
                            if(current.getType().equals("Pemasukan")){
                                listmasuk.add(current);
                            }
                        }

                        Adapter adapmasuk = new Adapter(record.this, listmasuk);
                        currentadaapter = adapmasuk;
                        recV.setAdapter(adapmasuk);
                        break;
                    case "Pengeluaran":
                        List<AddTransc> listkeluar = new ArrayList<>();
                        for(AddTransc current : adabterbaru.list){
                            if(current.getType().equals("Pengeluaran")){
                                listkeluar.add(current);
                            }
                        }

                        Adapter adapkeluar = new Adapter(record.this, listkeluar);
                        currentadaapter = adapkeluar;
                        recV.setAdapter(adapkeluar);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        filterbulan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Adapter saatini = currentadaapter;
                switch (adapterView.getSelectedItem().toString()){
                    case "Januari":
                        List<AddTransc> list1 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("1")){
                                list1.add(current);
                            }
                        }

                        Adapter adapt1 = new Adapter(record.this, list1);
                        recV.setAdapter(adapt1);
                        break;
                    case "Februari":
                        List<AddTransc> list2 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("2")){
                                list2.add(current);
                            }
                        }

                        Adapter adapt2 = new Adapter(record.this, list2);
                        recV.setAdapter(adapt2);
                        break;
                    case "Maret":
                        List<AddTransc> list3 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("3")){
                                list3.add(current);
                            }
                        }

                        Adapter adapt3 = new Adapter(record.this, list3);
                        recV.setAdapter(adapt3);
                        break;
                    case "April":
                        List<AddTransc> list4 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("4")){
                                list4.add(current);
                            }
                        }

                        Adapter adapt4 = new Adapter(record.this, list4);
                        recV.setAdapter(adapt4);
                        break;
                    case "Mei":
                        List<AddTransc> list5 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("5")){
                                list5.add(current);
                            }
                        }

                        Adapter adapt5 = new Adapter(record.this, list5);
                        recV.setAdapter(adapt5);
                        break;
                    case "Juni":
                        List<AddTransc> list6 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("6")){
                                list6.add(current);
                            }
                        }

                        Adapter adapt6 = new Adapter(record.this, list6);
                        recV.setAdapter(adapt6);
                        break;
                    case "Juli":
                        List<AddTransc> list7 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("7")){
                                list7.add(current);
                            }
                        }

                        Adapter adapt7 = new Adapter(record.this, list7);
                        recV.setAdapter(adapt7);
                        break;
                    case "Agustus":
                        List<AddTransc> list8 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("8")){
                                list8.add(current);
                            }
                        }

                        Adapter adapt8 = new Adapter(record.this, list8);
                        recV.setAdapter(adapt8);
                        break;
                    case "September":
                        List<AddTransc> list9 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("9")){
                                list9.add(current);
                            }
                        }

                        Adapter adapt9 = new Adapter(record.this, list9);
                        recV.setAdapter(adapt9);
                        break;
                    case "Oktober":
                        List<AddTransc> list10 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("10")){
                                list10.add(current);
                            }
                        }

                        Adapter adapt10 = new Adapter(record.this, list10);
                        recV.setAdapter(adapt10);
                        break;
                    case "November":
                        List<AddTransc> list11 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("11")){
                                list11.add(current);
                            }
                        }

                        Adapter adapt11 = new Adapter(record.this, list11);
                        recV.setAdapter(adapt11);
                        break;
                    case "Desember":
                        List<AddTransc> list12 = new ArrayList<>();
                        for(AddTransc current : saatini.list){
                            String [] bulandd = current.getDate().split("/");
                            if(bulandd[1].equals("12")){
                                list12.add(current);
                            }
                        }

                        Adapter adapt12 = new Adapter(record.this, list12);
                        recV.setAdapter(adapt12);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(record.this, Home.class));
        finish();
    }

    public void btnSubmit(View view) {

    }
}
