package android.lalita.com.myfi;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

/**
 * Created by lalita on 4/17/2018.
 */
@IgnoreExtraProperties
public class User {
    String id, key, gambar;
    int saldo;

    public User(String id, int saldo) {
        this.id = id;
        this.saldo = saldo;
    }

    public User(){

    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public HashMap<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("gambar",gambar);
        result.put("saldo", saldo);
        return result;
    }
}
