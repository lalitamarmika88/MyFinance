package android.lalita.com.myfi;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class transaction extends AppCompatActivity {
    private DatePickerDialog.OnDateSetListener mDateSetListener;
     EditText desc, amount; Spinner tipe; SharedPreferences shp; SharedPreferences.Editor editor;
    EditText tanggalan; String id; DatabaseReference datarefUser, datarefTrans; int saldosekarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        desc = findViewById(R.id.editDesc);
        amount = findViewById(R.id.editAmount);
        tipe = findViewById(R.id.tipeSpin);
        shp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = shp.edit();
        id = shp.getString("id",null);
        saldosekarang = shp.getInt("saldo", 0);
        tanggalan = findViewById(R.id.editTanggal);
        datarefUser = FirebaseDatabase.getInstance().getReference().child("user");
        datarefTrans = FirebaseDatabase.getInstance().getReference().child("transaksi");

        android.support.v7.widget.Toolbar tool = findViewById(R.id.toolbartransaksi);
        setSupportActionBar(tool);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);

        if(saldosekarang==0){
            AlertDialog.Builder bld = new AlertDialog.Builder(this);
            final View v = getLayoutInflater().inflate(R.layout.dialog_saldo, null);
            bld.setView(v);
            final EditText inputsaldo = v.findViewById(R.id.dialogsaldo);
            inputsaldo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    inputsaldo.removeTextChangedListener(this);

                    try {
                        String originalString = editable.toString();

                        Long longval;
                        if (originalString.contains(",")) {
                            originalString = originalString.replaceAll(",", "");
                        }
                        longval = Long.parseLong(originalString);

                        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                        formatter.applyPattern("#,###,###,###");
                        String formattedString = formatter.format(longval);

                        //setting text after format to EditText
                        inputsaldo.setText(formattedString);
                        inputsaldo.setSelection(inputsaldo.getText().length());
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }

                    inputsaldo.addTextChangedListener(this);
                }

            });
            bld.setTitle("Masukkan Saldo Awal!");
            bld.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialogInterface, int i) {
                    final int saldobaru = Integer.valueOf(inputsaldo.getText().toString().replaceAll(",", ""));
                    User saatini = new User(id, saldobaru);
                    saatini.setGambar(shp.getString("gambar", null));
                    datarefUser.child(shp.getString("key", null)).updateChildren(saatini.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(transaction.this, "Saldo updated!", Toast.LENGTH_SHORT).show();
                            editor.putInt("saldo", saldobaru);
                            saldosekarang = saldobaru;
                            editor.commit();
                        }
                    });
                }
            });
            bld.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            bld.create().show();
        }

        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                amount.removeTextChangedListener(this);

                try {
                    String originalString = editable.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    amount.setText(formattedString);
                    amount.setSelection(amount.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                amount.addTextChangedListener(this);
            }

            });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = day + "/" + month + "/" + year;
                tanggalan.setText(date);
            }
        };
    }

    public void bukakalender(View view) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                transaction.this,
                android.R.style.Theme_DeviceDefault_Dialog,
                mDateSetListener,
                year,month,day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void tambahtransaksi(View view) {
            if (amount.getText().toString().length()==0||desc.getText().toString().length()==0){
                AlertDialog.Builder bld = new AlertDialog.Builder(this);
                bld.setMessage("Please complete the field!");
                bld.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                bld.create().show();
            }else {
                int amounttransaction = Integer.valueOf(amount.getText().toString().replaceAll(",",""));
                final AddTransc baru = new AddTransc(desc.getText().toString(), amounttransaction, tipe.getSelectedItem().toString(), id, tanggalan.getText().toString());
                datarefTrans.push().setValue(baru).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(baru.getType().equals("Pengeluaran")){
                            saldosekarang-=baru.getAmount();
                        }else{
                            saldosekarang+=baru.getAmount();
                        }

                        editor.putInt("saldo", saldosekarang);
                        editor.commit();

                        User updatingsaldo = new User(id, saldosekarang);
                        updatingsaldo.setGambar(shp.getString("gambar", null));
                        datarefUser.child(shp.getString("key", null)).updateChildren(updatingsaldo.toMap()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(transaction.this, "Transaction Added!", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(transaction.this, Home.class));
                            }
                        });
                    }
                });
            }
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle("Canceling Transaction");
        bld.setMessage("Are you sure want to cancel inputing transaction?");
        bld.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(transaction.this, Home.class));
                finish();
            }
        });
        bld.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        bld.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.onBackPressed();
        }
        return true;
    }
}
