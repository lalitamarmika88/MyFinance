package android.lalita.com.myfi;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by lalita on 4/10/2018.
 */

@IgnoreExtraProperties
public class AddTransc {
    String description, type, user, date, key;
    int amount;
      public AddTransc(String description, int amount, String type, String user, String date){
        this.description=description;
        this.amount=amount;
        this.type=type;
        this.user=user;
        this.date=date;
    }

    public AddTransc(){

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
